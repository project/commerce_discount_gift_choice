<?php

/**
* @file
* Commerce discount gift choice views extra data.
*/

/**
* Alter the views data to enable some additional features for Commerce discount
* gift choice in views.
*/
function commerce_discount_gift_choice_views_data_alter(&$data) {
  if (isset($data['commerce_order'])) {
    // Expose the coupon form on the cart form.
    $data['commerce_order']['discount_gift_choice_form'] = array(
      'title' => t('Discount gift choice cart form'),
      'help' => t('Discount gift choice cart form'),
      'area' => array(
        'handler' => 'commerce_discount_gift_choice_handler_area_cart_form',
      ),
    );
  }
}
