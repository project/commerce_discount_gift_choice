<?php
/**
 * @file
 * Display a commerce coupon form field on the cart form.
 *
 * Most of this logic has been taken from commerce_coupon.checkout_pane.inc.
 * There is probably quite a bit of room for improvement here by reusing
 * code instead of copying it.
 */

class commerce_discount_gift_choice_handler_area_cart_form extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();

    $options['weight']['default'] = 99;

    return $options;
  }

  /**
   * Options form
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['empty']);

    $form['weight'] = array(
      '#type' => 'textfield',
      '#title' => t('Form item weight'),
      '#default_value' => $this->options['weight'],
      '#required' => TRUE,
    );
  }

  function options_validate(&$form, &$form_state) {
    $weight = $form_state['values']['options']['weight'];
    // Weight must be an integer:
    if (!is_null($weight )&& (!is_numeric($weight)) || (int) $weight != $weight) {
      form_set_error('options][weight', t('!name field must be an integer.', array('!name' => $form['weight']['#title'])));
    }
  }

  function render($values = FALSE) {
    // Render a Views form item placeholder.
    // This causes Views to wrap the View in a form.
    return '<!--form-item-' . $this->options['id'] . '-->';
  }

  /**
   * This handler never outputs data when the view is empty.
   */
  function views_form_empty($empty) {
    return $empty;
  }

  function views_form(&$form, &$form_state) {
    // Ensure this include file is loaded when the form is rebuilt from the
    // cache.
    $form_state['build_info']['files']['discount_gift_choice_form'] = drupal_get_path('module', 'commerce_discount_gift_choice') . '/includes/views/handlers/commerce_discount_gift_choice_handler_area_cart_form.inc';

    // First look for an order_id argument.
    foreach ($this->view->argument as $name => $argument) {
      if ($argument instanceof commerce_order_handler_argument_order_order_id) {
        // If it is single value...
        if (count($argument->value) == 1) {
          $order_id = reset($argument->value);
          break;
        }
      }
    }
    $order = !empty($order_id) ? commerce_order_load($order_id) : commerce_cart_order_load($GLOBALS['user']->uid);

    $form[$this->options['id']] = commerce_discount_gift_choice_choices_form($order);

    $form[$this->options['id']]['#tree'] = TRUE;
    $form[$this->options['id']]['#weight'] = $this->options['weight'];

    // Attach ajax if views ajax enabled.
    if ($this->view->use_ajax) {
      $form[$this->options['id']]['#ajax'] = array(
        'callback' => 'commerce_discount_gift_choice_handler_area_cart_form_callback',
        'wrapper' => 'commerce-cart-discount-gift-choice-ajax-wrapper',
      );
    }
    elseif (isset($form[$this->options['id']]['gift_choice_choose'])) {
      $form[$this->options['id']]['gift_choice_choose']['#type'] = 'submit';
      $form[$this->options['id']]['gift_choice_choose']['#submit'] = array('commerce_discount_gift_choice_handler_area_cart_form_submit');
    }
  }
}

/**
 * Submit: function commerce_discount_gift_choice_handler_area_cart_form
 */
function commerce_discount_gift_choice_handler_area_cart_form_submit($form, $form_state) {
  commerce_discount_gift_choice_choice_submit($form, $form_state);
}
