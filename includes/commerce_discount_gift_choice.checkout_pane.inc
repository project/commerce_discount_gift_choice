<?php
/**
 * @file
 * commerce_discount_gift_choice checkout panes.
 */

/**
 * This pane gives all gift available for the current order.
 */
function commerce_discount_gift_choice_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  // Get the gift of the commerce discount applied to the order,
  // and create a form with it.
  $pane_form = commerce_discount_gift_choice_choices_form($order);
  $pane_form['gift_choice_choose']['#ajax'] = array(
    'callback' => 'commerce_discount_gift_choice_pane_checkout_form_callback',
    'wrapper' => 'commerce-checkout-discount-gift-choice-ajax-wrapper',
  );
  return $pane_form;
}

/**
 * Save the selected gift into the order data.
 */
function commerce_discount_gift_choice_pane_checkout_form_callback($form, &$form_state) {
  // Save choices.
  commerce_discount_gift_choice_choice_submit($form, $form_state);

  // Re-render the cart summary.
  list($view_id, $display_id) = explode('|', variable_get('commerce_cart_contents_pane_view', 'commerce_cart_summary|default'));
  $cart_summary = commerce_embed_view($view_id, $display_id, array($form_state['order']->order_id));
  $commands[] = ajax_command_replace('.cart_contents .view', $cart_summary);

  // Re-render checkout_gift_choice_form pane.
  $gift_choice_pane = $form['checkout_gift_choice_form'];
  $commands[] = ajax_command_replace('#commerce-checkout-discount-gift-choice-ajax-wrapper', drupal_render($gift_choice_pane));

  // Allow other modules to alter the commands.
  drupal_alter('commerce_discount_gift_choice_choose_ajax', $commands, $form, $form_state);

  return array('#type' => 'ajax', '#commands' => $commands);
}
