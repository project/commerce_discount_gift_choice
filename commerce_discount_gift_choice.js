(function($) {
  Drupal.commerceDiscountGiftChoice = Drupal.commerceDiscountGiftChoice || {};

  Drupal.behaviors.commerceDiscountGiftChoice = {
    attach: function (context, settings) {

      // Initialize selected class.
      $('input.commerce-discount-gift-choice-input', context).once('commerce-discount-gift-choice').each(function() {
        Drupal.commerceDiscountGiftChoice(this);
        // Toggle selected class on selection.
        $(this).change(function() {
          Drupal.commerceDiscountGiftChoice(this);
        });
      });
    }
  };

  Drupal.commerceDiscountGiftChoice = function(element) {
    var $element = $(element);
    // If current input is checkbox, only toggle class
    if ($element.attr('type') == 'checkbox') {
      if (element.checked) {
        $element.closest('.form-item').addClass('selected');
      }
      else {
        $element.closest('.form-item').removeClass('selected');
      }
    }
    // Current input is radio, so remove all selected class and add to the
    // current selected.
    else if (element.checked) {
      $element.closest('div.commerce-discount-gift-choice-input').children('.form-item').removeClass('selected');
      $element.closest('.form-item').addClass('selected');
    }
  };

})(jQuery);
